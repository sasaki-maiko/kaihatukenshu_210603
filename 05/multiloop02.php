<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第5回　連想配列</title>
    </head>
    <body>
        <h1>第5回　連想配列</h1>
        <?php
        $pk01 = array(
            'name' => 'ピカチュウ',
            'type' => 'でんき',
            'from' => 'トキワのもり',
            'level' => '9',
        );
        $pk02 = array(
            'name' => 'ポッポ',
            'type' => 'ひこう',
            'from' => '3ばんどうろ',
            'level' => '4',
        );
        $pk03 = array(
            'name' => 'ゼニガメ',
            'type' => 'みず',
            'from' => 'マサラタウン',
            'level' => '12',
        );

        $pkall = array($pk01, $pk02, $pk03);

        foreach ($pkall as $each){
            echo 'なまえ：' . $each['name'] . ','
                . 'タイプ：' . $each['type'] . ','
                . 'であった場所：' . $each['from'] . ','
                . 'レベル：' . $each['level'] . '<br/>';
        }

        echo '<hr>';

        // テーブルで表示

        echo '<table border="1" frame="void">
            <th>なまえ</th>
            <th>タイプ</th>
            <th>であった場所</th>
            <th>レベル</th>';

        foreach ($pkall as $each){
            echo '<tr>' . '<td>' . $each['name'] . '</td>'
                . '<td>' . $each['type'] . '</td>'
                . '<td>' . $each['from'] . '</td>'
                . '<td>' . $each['level'] . '</td>'
                . '</tr>';
        }
        echo '</table>';

         ?>
    </body>
</html>
