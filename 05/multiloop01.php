<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第5回　連想配列</title>
    </head>
    <body>
        <h1>第5回　二次元配列</h1>
        <?php
        $team_a = array('あ', 'い', 'う', 'え', 'お');
        $team_b = array('a', 'b', 'c', 'd', 'e');
        $team_c = array('1', '2', '3', '4', '5');
        $team_d = array('α', 'β', 'γ', 'δ', 'ε');
        $team_e = array('親', '人', '中', '薬', '小');

        $team_all = array($team_a, $team_b, $team_c, $team_d, $team_e);

        echo '<pre>';
        var_dump($team_all);
        echo'</pre>';

        echo '<hr>';

            // foreachを二重にして全件表示させる

        foreach($team_all as $hairetu){
            foreach($hairetu as $abc){
                echo $abc;
            }
            echo '<br/>';
        }

         ?>

    </body>
</html>
