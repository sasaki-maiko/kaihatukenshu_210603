<!DOCTYPE html>
<link rel='stylesheet' href='./include/style.css'>
<?php
    // common
    // include('./include/functions.php');
    $DB_DSN = 'mysql:host=localhost; dbname=msasaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    // 条件文を作成する。
    // 検索条件があるときのみWHERE文を作成する。
    $where_str = "";
    $cond_ryouriname = "";
    $cond_genre = "";
    $cond_pricefrom = "";
    $cond_priceto = "";

    if (isset($_GET['ryouriname']) && !empty($_GET['ryouriname'])){
        $where_str .= " AND ryouriname LIKE '%" . $_GET['ryouriname'] . "%'";
        $cond_ryouriname = $_GET['ryouriname'];
    }
    if (isset($_GET['genre']) && !empty($_GET['genre'])){
        $where_str .= " AND genre = '" . $_GET['genre'] . "'";
        $cond_genre = $_GET['genre'];
    }
    if (isset($_GET['price_from']) && !empty($_GET['price_from'])){
        $where_str .= " AND price >= '" . $_GET['price_from'] . "'";
        $cond_price_from = $_GET['price_from'];
    }
    if (isset($_GET['price_to']) && !empty($_GET['price_to'])){
        $where_str .= " AND price <= '" . $_GET['price_to'] . "'";
        $cond_price_to = $_GET['price_to'];
    }

/*
    // WHERE文以降を作成する。
    if($where_str != ""){
    // WHEREを頭につけて、語尾の4文字を解除する。
    $where_str = " WHERE " . substr($where_str, 0, -4)
    }
*/
    // SQLの基本部分。全件検索のときはこれだけ
    $query_str = "SELECT * FROM test WHERE 1";

    // 基本部分にwhere文をくっつける.条件がなければ$where_strは空白だからそのままつく
    $query_str .= $where_str;

    //$query_str = "SELECT * FROM `test` WHERE ryouriname LIKE '%の%' AND genre = 'おつまみ'";

    echo $query_str;
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchall();

?>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>居酒屋ウェブレッジ水道橋店</title>
    </head>
    <body>
        <h1>
            居酒屋SQL
        </h1>
            <form method='get' action='test02.php'>
                <table>
                    <tr>
                        <th>
                            名前
                        </th>
                        <td>
                            <input type='text' name='ryouriname' value="<?php if(empty($_GET['ryouriname'])) echo $_GET['ryouriname']?>">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            ジャンル
                        </th>
                        <td>
                            <select name='genre'>
                                <option value='' style=';' <?php if(empty($_GET['genre'])) echo 'selected';?>>
                                    --選択してください--
                                </option>
                                <option value='炒め物' <?php echo $_GET['genre'] == '炒め物' ? 'selected' : '';?>>
                                    炒め物
                                </option>
                                <option value='アジアン' <?php echo $_GET['genre'] == 'アジアン' ? 'selected' : '';?>>
                                    アジアン
                                </option>
                                <option value='おつまみ' <?php echo $_GET['genre'] == 'おつまみ' ? 'selected' : '';?>>
                                    おつまみ
                                </option>
                                <option value='お食事' <?php echo $_GET['genre'] == 'お食事' ? 'selected' : '';?>>
                                    お食事
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            値段
                        </th>
                        <td>
                            <input type='number' name='price_from' value="<?php echo $_GET['price_from'];?>">
                            ～
                            <input type='number' name='price_to' value="<?php echo $_GET['price_to'];?>">
                            円
                        </td>
                    </tr>
                </table>
                <input type='submit' name='bsubmit' value='検索'>
                <input type='reset' name='breset' value='リセット'>
            </form>
            <hr>

            <?php

            //ここからテーブルに出力する

                echo '<table border="1" frame="void" rules="rows">
                    <th>
                        No.
                    </th>
                    <th>
                        料理名
                    </th>
                    <th>
                        ジャンル
                    </th>
                    <th>
                        値段
                    </th>
                    <th>
                        メモ
                    </th>
                    ';

                        foreach($result as $each){
                            echo '<tr>' . '<td>' . $each['ID'] . '</td>'
                                . '<td>' . $each['ryouriname'] . '</td>'
                                . '<td>' . $each['genre'] . '</td>'
                                . '<td>' . $each['price'] . '</td>'
                                . '<td>' . $each['memo'] . '</td>'
                                . '</tr>';
                        }

                echo '</table>';

             ?>

    </body>
</html>
