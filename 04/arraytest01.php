<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第4回　配列</title>
    </head>
    <body>
        <h1>第4回　配列</h1>
        <?php
        // テキスト写経
        $fruit = array('りんご', 'すいか', 'みかん', 'なし', 'イチゴ', 'かき');

        echo $fruit[3]; // なし　と表示される

        echo $fruit[0]; // りんご　と表示される

        echo $fruit[9]; //エラーになる

        $fruit[2] = 'いちじく'; // 上書きされる

        var_dump($fruit); // 配列の中身がすべて表示される

        ?>
        <pre>
        <?php var_dump($fruit);
        ?>
        </pre>

        <?php
        for($i=0; $i < count($fruit) ; $i++){
            echo $fruit[$i] . '<br/>';
        }
        ?>

        <?php
        echo '<hr>';

        foreach($fruit as $each){
            echo $each . '<br/>';
        }

        echo '<hr>';

        foreach($fruit as $key => $value){
            echo $key . '番目の要素は' . $value . 'です。<br/>';
        }
         ?>
    </body>
</html>
