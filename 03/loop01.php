<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第3回　分岐と繰り返し</title>
    </head>
    <body>
        <h1>第3回　分岐と繰り返し</h1>
        <table>
            <th>
                無限テーブル
            </th>
            <tr>
                <td>
                    <form action = 'loop01.php' method = 'GET'>
                    <input type = number name='looptr' style='text-align: right; width:50px' value='1' min='1' placeholder='数字'>行のテーブルを作成
                </td>
            </tr>
            <tr>
                <td>
                    <input type=reset value=' リセット '>　<input type=submit value=' 送信 '>
                </td>
            </tr>
            <tr>
                <td>
                    <hr>
                </td>
            </tr>
        </table>

        <table border='1' rules='rows' frame='void'>
            <?php
              for($i=0; $i < $_GET['looptr']; $i++){
                echo "
                    <tr>
                        <td>
                            インコ
                        </td>
                        <td>
                            ペンギン
                        </td>
                        <td>
                            フクロウ
                        </td>
                        <td>
                            フィンチ
                        </td>
                        <td>
                            エミュー
                        </td>
                    </tr>
                    ";
                }
            ?>
        </table>
    </body>
</html>
