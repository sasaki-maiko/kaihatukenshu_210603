<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第3回　分岐と繰り返し</title>
    </head>
    <body>
        <h1>第3回　分岐と繰り返し</h1>
        <?php
            // idを設定
            $id = "hogehoge";
            // パスワードを設定
            $pass = "xxxxxx";

            if($_POST['id'] == $id AND $_POST['pass'] == $pass){
            /* ログイン成功の処理を書く */
                echo 'ログイン成功';
            } else {
            /* ログイン失敗の処理を書く */
                echo 'ログイン失敗しました';
            }
        ?>
    </body>
</html>
