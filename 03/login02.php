<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第3回　分岐と繰り返し</title>
    </head>
    <body>
        <h1>第3回　分岐と繰り返し</h1>
        <form action = 'result02.php' method = 'POST'>
        <table border='1' rules='rows' frame='void'>
            <th colspan=2>
                通常会員orゴールド会員　半角英数字で
            </th>
            <tr>
                <td>
                    ID
                    <input type = text name='id' placeholder='半角英数字' required>
                </td>
                <td>
                    pass
                    <input type = text name='pass' placeholder='半角英数字' required>
                </td>
            </tr>
            <tr>
                <td colspan=2 style='text-align: right;'>
                    <input type=reset value=' リセット '>　<input type=submit value=' 送信 '>
                </td>
            </tr>
        </table>
        </form>
    </body>
</html>
