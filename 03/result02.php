<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第3回　分岐と繰り返し</title>
    </head>
    <body>
        <h1>第3回　分岐と繰り返し</h1>
        <?php
            // ゴールド会員
            $gold_pw = "xxxxxxx";
            $gold_id = "hogehogegold";

            // ノーマル会員
            $reg_pw = "ooooooo";
            $reg_id = "hogehogeregular";

            // 分岐の処理を書く
            if($_POST['id'] == $gold_id AND $_POST['pass'] == $gold_pw){
                echo 'ゴールド会員ページ';

            }elseif($_POST['id'] == $reg_id AND $_POST['pass'] == $reg_pw){
                echo 'レギュラー会員ページ';

            }else{
            // ログイン失敗
                echo 'ログイン失敗';
            }

        ?>
    </body>
</html>
