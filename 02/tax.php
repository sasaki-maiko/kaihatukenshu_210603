<!DOCTYPE html>
<html>
    <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第二回課題、フォーム部品練習</title>
    </head>
    <body>
        <h1>税計算</h1>
        <table border='1' style='border-collapse:collapse;'>
        	<tbody>
        		<tr>
        			<th style=width:200px;>
                        商品名
                    </th>
        			<th style=width:200px;>
                        価格（単位：円、税抜）
                    </th>
        			<th style=width:80px;>
                        個数
                    </th>
        			<th style=width:120px;>
                        税率
                    </th>
        		</tr>
        		<tr>
                <form method='POST' action='tax.php'>
        			<td>
                        <input type='text' name='shohin1' size='20' placeholder='商品名1'>
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:100px;' name='kakaku1' value='0' min='0'>
                        円
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:50px;' value='0' name='kazu1' min='0'>
                        個
                    </td>
        			<td>
                        <label>
                            <input type=radio name='z1' value='8' checked>
                            8％
                        </label>
                        <label>
                            <input type=radio name='z1' value='10'>
                            10％
                        </label>
                    </td>
        		</tr>
        		<tr>
        			<td>
                        <input type='text' name='shohin2' size='20' placeholder='商品名2'>
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:100px;' name='kakaku2' value='0' min='0'>
                        円
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:50px' value='0' name='kazu2' min='0'>
                        個
                    </td>
        			<td>
                        <label>
                            <input type=radio name='z2' value='8' checked>
                            8％
                        </label>
                        <label>
                            <input type=radio name='z2' value='10'>
                            10％
                        </label>
                    </td>
        		</tr>
        		<tr>
        			<td>
                        <input type='text' name='shohin3' size='20' placeholder='商品名3'>
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:100px;' name='kakaku3' value='0' min='0'>
                        円
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:50px' value='0' name='kazu3' min='0'>
                        個
                    </td>
        			<td>
                        <label>
                            <input type=radio name='z3' value='8' checked>
                            8％
                        </label>
                        <label>
                            <input type=radio name='z3' value='10'>
                            10％
                        </label>
                    </td>
        		</tr>
        		<tr>
        			<td>
                        <input type='text' name='shohin4' size='20' placeholder='商品名4'>
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:100px;' name='kakaku4' value='0' min='0'>
                        円
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:50px' value='0' name='kazu4' min='0'>
                        個
                    </td>
        			<td>
                        <label>
                            <input type=radio name='z4' value='8' checked>
                            8％
                        </label>
                        <label>
                            <input type=radio name='z4' value='10'>
                            10％
                        </label>
                    </td>
        		</tr>
        		<tr>
        			<td>
                        <input type='text' name='shohin5' size='20' placeholder='商品名5'>
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:100px;' name='kakaku5' value='0' min='0'>
                        円
                    </td>
        			<td>
                        <input type='number' style='text-align: right; width:50px' value='0' name='kazu5' min='0'>
                        個
                    </td>
        			<td>
                        <label>
                            <input type=radio name='z5' value='8' checked>
                            8％
                        </label>
                        <label>
                            <input type=radio name='z5' value='10'>
                            10％
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan=4>
                        <center>
                            <input type=reset value=' リセット '>　<input type=submit value=' 送信 '>
                        </center>
                    </form>
                    </td>
                </tr>
        	</tbody>
        </table>
        <table border='0' style='border-collapse:collapse;'>
            <tbody>
                <tr>
                    <th style='width:200px;'>
                        商品名
                    </th>
                    <th style='width:200px;'>
                        価格（単位：円、税抜）
                    </th>
                    <th style=width:80px;>
                        個数
                    </th>
                    <th style=width:120px;>
                        税率
                    </th>
                    <th>
                        小計（単位：円）
                    </th>
                </tr>
                <tr>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['shohin1'];
                        ?>
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kakaku1']);
                        ?>
                        円
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kazu1']);
                        ?>
                        個
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['z1'];
                        ?>
                        ％
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kakaku1 = $_POST['kakaku1'];
                            $kazu1 = $_POST['kazu1'];
                            $z1 = $_POST['z1'];
                            $kei1 = $kakaku1 * $kazu1 * (1 + ($z1 / 100));
                            echo number_format($kei1);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['shohin2'];
                        ?>
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kakaku2']);
                        ?>
                        円
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kazu2']);
                        ?>
                        個
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['z2'];
                        ?>
                        ％
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kakaku2 = $_POST['kakaku2'];
                            $kazu2 = $_POST['kazu2'];
                            $z2 = $_POST['z2'];
                            $kei2 = $kakaku2 * $kazu2 * (1 + ($z2 / 100));
                            echo number_format($kei2);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['shohin3'];
                        ?>
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kakaku3']);
                        ?>
                        円
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kazu3']);
                        ?>
                        個
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['z3'];
                        ?>
                        ％
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kakaku3 = $_POST['kakaku3'];
                            $kazu3 = $_POST['kazu3'];
                            $z3 = $_POST['z3'];
                            $kei3 = $kakaku3 * $kazu3 * (1 + ($z3 / 100));
                            echo number_format($kei3);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['shohin4'];
                        ?>
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kakaku4']);
                        ?>
                        円
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kazu4']);
                        ?>
                        個
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['z4'];
                        ?>
                        ％
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kakaku4 = $_POST['kakaku4'];
                            $kazu4 = $_POST['kazu4'];
                            $z4 = $_POST['z4'];
                            $kei4 = $kakaku4 * $kazu4 * (1 + ($z4 / 100));
                            echo number_format($kei4);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['shohin5'];
                        ?>
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kakaku5']);
                        ?>
                        円
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo number_format($_POST['kazu5']);
                        ?>
                        個
                    </td>
                    <td style='text-align: right;'>
                        <?php
                        echo $_POST['z1'];
                        ?>
                        ％
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kakaku5 = $_POST['kakaku5'];
                            $kazu5 = $_POST['kazu5'];
                            $z5 = $_POST['z5'];
                            $kei5 = $kakaku5 * $kazu5 * (1 + ($z5 / 100));
                            echo number_format($kei5);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan=5>
                        <hr size='1' noshade>
                    </td>
                </tr>
                <tr>
                    <td colspan=4 style='text-align: right;'>合計
                    </td>
                    <td style='text-align: right;'>
                        <?php
                            $kei = $kei1 + $kei2 + $kei3 + $kei4 + $kei5;
                            echo number_format($kei);
                        ?>
                        円
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
